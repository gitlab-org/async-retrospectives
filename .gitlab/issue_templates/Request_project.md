Create a new project under https://gitlab.com/gl-retrospectives.

<!-- Fill in the name of the project to be created under https://gitlab.com/gl-retrospectives -->

- [ ] Project name: `<project-name>`

<!-- see list of Owners at https://gitlab.com/groups/gl-retrospectives/-/group_members -->
/assign @[REPLACE-WITH-ADEQUATE-DIRECTOR-OF-ENGINEERING]
