require 'gitlab_api'
require 'release_collection'

RSpec.describe ReleaseCollection do
  let(:client) { GitlabApi.new(token: 'fake_token') }
  subject { ReleaseCollection.new(api_client: client) }

  let(:milestones) do
    [
      {
        'start_date' => (Date.today - 3).to_s,
        'title' => '30.5'
      },
      {
        'start_date' => (Date.today - 31).to_s,
        'title' => '30.4'
      },
      {
        'start_date' => (Date.today - 365).to_s,
        'title' => '20.6'
      },
      {
        'start_date' => (Date.today - 365).to_s,
        'title' => '15.0'
      }
    ]
  end

  before do
    allow(client).to receive(:get).with('groups/gitlab-org/milestones?state=active').and_return(milestones)
  end

  describe '#all' do
    context 'when response is successful' do
      it 'returns a list of milestones' do
        expect(subject.all).to eq(milestones.sort_by { |m| m['start_date'] })
      end
    end
  end

  describe '#current' do
    context 'when response is successful' do
      it 'returns a single, correct milestone' do
        expect(subject.current).to eq(milestones[0])
      end
    end
  end

  describe '#at' do
    context 'when response is successful' do
      it 'returns a single, correct milestone' do
        expect(subject.at('15.0')).to eq(milestones[3])
      end
    end
  end

  describe '#previous' do
    context 'when response is successful' do
      it 'returns a single, correct milestone' do
        expect(subject.previous).to eq(milestones[1])
      end
    end
  end
end
