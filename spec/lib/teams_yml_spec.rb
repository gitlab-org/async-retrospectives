require 'team'

RSpec.describe Team do
  let(:all_teams) { Team.all }

  describe 'all_teams' do

    it 'should have a unique project' do
      number_of_teams = all_teams.size
      number_of_unique_projects = all_teams.collect{ |team| team.project}.uniq.size

      expect(number_of_teams).to eq(number_of_unique_projects),
                                 "expected each team project to be unique (#{number_of_teams} teams, #{number_of_unique_projects} unique projects in teams.yml)"
    end

    it 'should at least one team-member if mention_team_on_creation is used' do
      mention_teams = all_teams.select(&:mention_team_on_creation)

      mention_teams.each do |team|
        mentioned = Team::TEAM_MEMBER_FIELDS.each_with_object([]) do |f, acc|
          acc.concat(team[f].to_a)
        end

        expect(mentioned).not_to be_empty, "team #{team.name} defined mention_team_on_create as true but there are no team members listed under valid keys (#{Team::TEAM_MEMBER_FIELDS})"
      end
    end
  end

end
