require 'team'

RSpec.describe Team do
  let(:teams) do
    {
      'Plan' => {
        'label' => 'plan',
        'project' => 'plan',
        'owner' => 'smcgivern',
        'mention_team' => false,
        'mention_team_on_creation' => false,
        'create_discussions' => false,
        'create_individual_discussions' => true
      },
      'Growth' => {
        'label' => 'devops::growth',
        'project' => 'growth',
        'owner' => %w{pcalder jayswain},
        'template' => 'growth',
        'mention_team' => false,
        'mention_team_on_creation' => false,
        'create_discussions' => false,
        'create_individual_discussions' => true
      }
    }
  end

  before do
    described_class.reset!
  end

  describe '.find' do
    before do
      allow(YAML).to receive(:load_file).and_return(teams)
    end

    context 'when the team name does not exist' do
      it 'raises a KeyError' do
        expect { described_class.find('Foo') }.to raise_error(KeyError)
      end
    end

    context 'when the team name exists' do
      it 'returns a TeamInfo struct with the team details' do
        expect(described_class.find('Plan'))
          .to have_attributes(label: 'plan',
                              project: 'plan',
                              template: 'default',
                              owner: 'smcgivern',
                              mention_team: false,
                              mention_team_on_creation: false,
                              create_discussions: false,
                              create_individual_discussions: true)
      end
    end
  end

  describe '.all' do
    before do
      allow(YAML).to receive(:load_file).and_return(teams)
    end

    it 'returns TeamInfo structs for all teams' do
      expect(described_class.all).to all(be_a(described_class::TeamInfo))
      expect(described_class.all.last)
        .to have_attributes(label: 'devops::growth',
                            project: 'growth',
                            template: 'growth',
                            owner: %w{pcalder jayswain},
                            mention_team: false,
                            mention_team_on_creation: false,
                            create_discussions: false,
                            create_individual_discussions: true)
    end
  end

  describe 'the TeamInfo struct' do
    before do
      allow(YAML).to receive(:load_file).and_return(teams)
    end

    describe '#confidential' do
      let(:team_info) { described_class.find('Plan') }

      it 'returns false when confidential is not specified' do
        expect(team_info.confidential?).to be(false)
      end
    end
  end

  describe 'the teams in teams.yml' do
    it 'loads all teams successfully' do
      expected_attributes = described_class::FIELDS.zip { anything }.to_h

      expect(described_class.all).to all(have_attributes(expected_attributes))
    end

    it 'does not have teams where create_individual_discussions is set with create_discussions or mention_team' do
      invalid_teams = described_class.all.select do |team|
        team.create_individual_discussions &&
          (team.create_discussions || team.mention_team)
      end.map(&:name)

      expect(invalid_teams).to be_empty
    end
  end
end
