require_relative '../retrospective'
require 'timecop'

describe "#run_for_all_teams" do
  context "with error" do
    let(:test_lambda) do
      lambda { |team| raise StandardError }
    end
    it 'returns a 1 exit code' do
      expect(run_for_all_teams(&test_lambda)).to eq(1)
    end
  end

  context "with no errors" do
    let(:test_lambda) do
      lambda { |team| }
    end
    it 'returns a 0 exit code' do
      expect(run_for_all_teams(&test_lambda)).to eq(0)
    end
  end
end

describe "#correct_date?" do
  let (:milestone) do
    {
      'start_date' => '2023-08-18',
      'due_date' => '2023-09-17',
      'title' => '16.4'
    }
  end

  before { Timecop.freeze(current_time) }

  context 'when action is create' do
    context 'when it is 9 days since the start of the milestone' do
      let(:current_time) { Date.parse('2023-08-27') }

      it 'is expected to be true' do
        expect(correct_date?('create', milestone)).to be true
      end
    end

    context 'when it is any date other than 9 days since the start of milestone' do
      let(:current_time) { Date.parse('2023-08-26') }

      it 'is expected to be false' do
        expect(correct_date?('create', milestone)).to be false
      end
    end
  end

  context 'when action is update' do
    context 'when it is 4 days since the end of the last milestone' do
      let(:current_time) { Date.parse('2023-09-21') }

      it 'is expected to be true' do
        expect(correct_date?('update', milestone)).to be true
      end
    end

    context 'when it is any date other than 4 days since the end of the last milestone' do
      let(:current_time) { Date.parse('2023-09-22') }

      it 'is expected to be false' do
        expect(correct_date?('update', milestone)).to be false
      end
    end
  end
end
