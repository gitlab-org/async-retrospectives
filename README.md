# Async retrospective issue creator

Creates issues for use in [team retrospectives] automatically.

For more information, read the [blog post]!

[blog post]: https://about.gitlab.com/2019/03/07/how-we-used-gitlab-to-automate-our-monthly-retrospectives/

## How it works

This project has [scheduled pipelines] that run every day. On specific dates,
they trigger actions related to the administration of retrospectives:

* **9 Days after the milestone starts** it creates a 'skeleton issue'. This 
  contains the general description of what the retrospective is for.
* **4 Days after the milestone ends**, it optionally updates the existing 
  issue's description with:
    * All ~Deliverable issues the team shipped that month.
    * Any ~missed-deliverable issues.
    * Any ~follow-up issues from previous retrospectives that are still
      open.
    * Value Stream Analytics (VSA) for the current release cycle.

  It also mentions the team to encourage them to contribute, and
  optionally creates some discussions on the issue for the team to use
  during the retrospective.

[team retrospectives]: https://about.gitlab.com/handbook/engineering/management/team-retrospectives/
[scheduled pipelines]: https://gitlab.com/gitlab-org/async-retrospectives/pipeline_schedules

### Forcing actions outside of this schedule

Sometimes you want to create or update retrospective issues outside of this
schedule. For example, you might want to generate an issue for a recently
created team, or the most recent update failed and you want to re-run it more
than a day later.

This project has two jobs you can run at any time:
  * `force-create` creates an issue for a single team.
  * `force-update` updates an issue for a single team.

Both of these jobs must be manually triggered from the `master` branch with the
variable `TEAM_NAME` provided. `TEAM_NAME` must exactly match the entry in 
[`teams.yml`](teams.yml).

You can also force the action for every team by specifying the following variables
and rerunning the `perform` job:

```shell
DRY_RUN=true #remove after you tried it and you're confident it doesn't do the wrong thing
FORCE=true
CMD=create/update #leave only one
```

It's important to use the `CMD` argument exactly as described. Otherwise it will default to creating a new retrospective issue for every team.

### Access Tokens

The project requires two access tokens to function. One is a Group Access Token
with read and write permissions for the [`gl-retrospectives`] group, which
is used to create the retrospective issues. The other is a read-only token for
the [GitLab-Bot] user, which is used to query issues to be included in the
updated retrospective description.

These are defined as [CI/CD Variables](https://gitlab.com/gitlab-org/async-retrospectives/-/settings/ci_cd)
so that any maintainer of this project can update them, and are named
`GITLAB_WRITE_API_TOKEN` and `GITLAB_BOT_API_TOKEN`, respectively.

If these tokens are rotated or expire the pipeline will fail with `401` errors,
either when querying or creating issues. The fix is to acquire a new token and
update the corresponding CI/CD Variable, then re-run the pipeline.

[GitLab-Bot]: https://gitlab.com/gitlab-bot
[CI/CD Variables]: https://gitlab.com/gitlab-org/async-retrospectives/-/settings/ci_cd
[`gl-retrospectives`]: https://gitlab.com/groups/gl-retrospectives/-/settings/access_tokens

## Adding a new team

To add a new team, we need to:

1. Create a project for that team in [gl-retrospectives]. If you don't have
   permissions to create it, [open an issue](https://gitlab.com/gitlab-org/async-retrospectives/issues/new?issuable_template=Request_project&issue[title]=New%20project%20request).
2. Update [`teams.yml`](teams.yml) with the team info. See the comments
   in that file for information on the various options.

Then, the pipeline schedules above will take care of the rest!

[gl-retrospectives]: https://gitlab.com/gl-retrospectives

### Templates

We use [ERB] templates for creating issues. These templates have access
to the variables you can see in the [default template], as well as a
proc, `include_template`, that will render and insert another template
in its place. So this:

```erb
This is the main template.

<%= include_template.call('other_template') %>
```

Will contain the result of rendering `other_template`.

[ERB]: https://ruby-doc.org/stdlib/libdoc/erb/rdoc/ERB.html
[default template]: templates/default.erb

## Running locally

```shell
$ bundle
$ bundle exec ruby retrospective.rb # Shows options
```

For example, this will write the issue description for a Plan retrospective to
standard output. (To actually create an issue, remove `--dry-run`.)

```shell
$ bundle exec ruby retrospective.rb create --dry-run --read-token="$GITLAB_API_TOKEN" --write-token=$GITLAB_WRITE_API_TOKEN --team=Plan
```

The read-token supplied should have read access to all groups. The write token
should have write access to just one group, the `gl-retrospectives` group, which
contains projects for each team. 

## The gl-retrospectives group

The [gl-retrospectives group][gl-retrospectives] is a new top-level group, not
inside gitlab-com or gitlab-org.

This is because retrospectives may be confidential to the stage group or team
while they're being discussed. If the project was in a group like gitlab-com,
then everyone with access to gitlab-com could see those confidential issues.

To work around this, we have a separate top-level group with limited access.

## Contributing

Contributions are welcome! See [CONTRIBUTING.md] and the [LICENSE].
Contributions that add or change teams or templates do not require
review. Everything else must be reviewed by a codeowner.

[CONTRIBUTING.md]: CONTRIBUTING.md
[LICENSE]: LICENSE
